# Minimal setup to test dbus for kunifiedpush

## Build and run the service
```
gcc -o dbus_service service.c $(pkg-config --cflags --libs gio-2.0)
./dbus_service
```
## Build and run the client
```
gcc -o dbus_client client.c $(pkg-config --cflags --libs gio-2.0)
./dbus_client --dbus-activated
```

### To unregister
```
./dbus_client --unregister
```