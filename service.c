#include <gio/gio.h>

#define SERVICE_NAME "org.gtk.kunifiedpush.demo_notifier"
#define OBJECT_PATH "/org/gtk/kunifiedpush/demo_notifier"
#define INTERFACE_NAME "org.gtk.kunifiedpush.Connector"

static GDBusNodeInfo *introspection_data = NULL;

static const gchar introspection_xml[] =
    "<node>"
    "  <interface name='" INTERFACE_NAME "'>"
    "    <method name='RegisterClient'>"
    "      <arg type='s' name='client' direction='in'/>"
    "    </method>"
    "    <method name='UnregisterClient'>"
    "      <arg type='s' name='client' direction='in'/>"
    "    </method>"
    "    <signal name='newMessage'>"
    "      <arg type='s' name='message'/>"
    "    </signal>"
    "    <signal name='newEndpoint'>"
    "      <arg type='s' name='endpoint'/>"
    "    </signal>"
    "    <signal name='Unregistered'>"
    "      <arg type='s' name='client'/>"
    "    </signal>"
    "  </interface>"
    "</node>";

static void handle_method_call(GDBusConnection *connection,
                               const gchar *sender,
                               const gchar *object_path,
                               const gchar *interface_name,
                               const gchar *method_name,
                               GVariant *parameters,
                               GDBusMethodInvocation *invocation,
                               gpointer user_data)
{
    if (g_strcmp0(method_name, "RegisterClient") == 0) {
        const gchar *client;
        g_variant_get(parameters, "(&s)", &client);
        g_print("RegisterClient called with: %s\n", client);
        g_dbus_method_invocation_return_value(invocation, NULL);
    } else if (g_strcmp0(method_name, "UnregisterClient") == 0) {
        const gchar *client;
        g_variant_get(parameters, "(&s)", &client);
        g_print("UnregisterClient called with: %s\n", client);
        g_dbus_method_invocation_return_value(invocation, NULL);
    }
}

static const GDBusInterfaceVTable interface_vtable = {
    handle_method_call,
    NULL,
    NULL
};

static void on_bus_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    guint registration_id;

    registration_id = g_dbus_connection_register_object(connection,
                                                        OBJECT_PATH,
                                                        introspection_data->interfaces[0],
                                                        &interface_vtable,
                                                        NULL,
                                                        NULL,
                                                        NULL);
    g_assert(registration_id > 0);

    g_print("Service started and registered object path\n");
}

static void on_name_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    g_print("Name acquired: %s\n", name);
}

static void on_name_lost(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    g_print("Name lost: %s\n", name);
}

int main(int argc, char *argv[])
{
    GMainLoop *loop;
    guint owner_id;

    introspection_data = g_dbus_node_info_new_for_xml(introspection_xml, NULL);
    g_assert(introspection_data != NULL);

    owner_id = g_bus_own_name(G_BUS_TYPE_SESSION,
                              SERVICE_NAME,
                              G_BUS_NAME_OWNER_FLAGS_NONE,
                              on_bus_acquired,
                              on_name_acquired,
                              on_name_lost,
                              NULL,
                              NULL);

    loop = g_main_loop_new(NULL, FALSE);
    g_main_loop_run(loop);

    g_bus_unown_name(owner_id);
    g_dbus_node_info_unref(introspection_data);

    return 0;
}
