#include <gio/gio.h>

#define SERVICE_NAME "org.gtk.kunifiedpush.demo_notifier"
#define OBJECT_PATH "/org/gtk/kunifiedpush/demo_notifier"
#define INTERFACE_NAME "org.gtk.kunifiedpush.Connector"

static GDBusProxy *proxy = NULL;

static void on_signal(GDBusProxy *proxy, gchar *sender_name, gchar *signal_name, GVariant *parameters, gpointer user_data)
{
    if (g_strcmp0(signal_name, "newMessage") == 0) {
        const gchar *message;
        g_variant_get(parameters, "(&s)", &message);
        g_print("Received new message: %s\n", message);
    } else if (g_strcmp0(signal_name, "newEndpoint") == 0) {
        const gchar *endpoint;
        g_variant_get(parameters, "(&s)", &endpoint);
        g_print("Received new endpoint: %s\n", endpoint);
    } else if (g_strcmp0(signal_name, "Unregistered") == 0) {
        const gchar *client;
        g_variant_get(parameters, "(&s)", &client);
        g_print("Client unregistered: %s\n", client);
    }
}

static void call_register_client(const gchar *client)
{
    GError *error = NULL;
    GVariant *result;

    result = g_dbus_proxy_call_sync(proxy,
                                    "RegisterClient",
                                    g_variant_new("(s)", client),
                                    G_DBUS_CALL_FLAGS_NONE,
                                    -1,
                                    NULL,
                                    &error);
    if (error) {
        g_print("Error calling RegisterClient: %s\n", error->message);
        g_clear_error(&error);
    } else {
        g_print("Client registered: %s\n", client);
    }
}

static void call_unregister_client(const gchar *client)
{
    GError *error = NULL;
    GVariant *result;

    result = g_dbus_proxy_call_sync(proxy,
                                    "UnregisterClient",
                                    g_variant_new("(s)", client),
                                    G_DBUS_CALL_FLAGS_NONE,
                                    -1,
                                    NULL,
                                    &error);
    if (error) {
        g_print("Error calling UnregisterClient: %s\n", error->message);
        g_clear_error(&error);
    } else {
        g_print("Client unregistered: %s\n", client);
    }
}

int main(int argc, char *argv[])
{
    GMainLoop *loop;
    GError *error = NULL;

    proxy = g_dbus_proxy_new_for_bus_sync(G_BUS_TYPE_SESSION,
                                          G_DBUS_PROXY_FLAGS_NONE,
                                          NULL,
                                          SERVICE_NAME,
                                          OBJECT_PATH,
                                          INTERFACE_NAME,
                                          NULL,
                                          &error);
    if (error) {
        g_print("Error creating proxy: %s\n", error->message);
        g_clear_error(&error);
        return 1;
    }

    g_signal_connect(proxy, "g-signal", G_CALLBACK(on_signal), NULL);

    call_register_client("TestClient");
    call_unregister_client("TestClient");

    loop = g_main_loop_new(NULL, FALSE);
    g_main_loop_run(loop);

    g_object_unref(proxy);
    g_main_loop_unref(loop);

    return 0;
}
